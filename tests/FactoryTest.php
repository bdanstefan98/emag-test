<?php

declare(strict_types=1);
namespace App\Tests;

use App\Factory\HeroFactory;
use App\Factory\MobFactory;
use PHPUnit\Framework\TestCase;

final class FactoryTest extends TestCase
{
    public function testHeroMaxStrength()
    {
        $hero = HeroFactory::getHero();
        $this->assertTrue($hero->getStrength() <= 80);
    }

    public function testHeroMinHealth()
    {
        $hero = HeroFactory::getHero();
        $this->assertTrue($hero->getHealth() > 70);
    }

    public function testMobLuckNotEquals()
    {
        $mob = MobFactory::getMob();
        $this->assertNotTrue($mob->getLuck() > 70);
    }

    public function testMobSkills()
    {
        $mob = MobFactory::getMob();
        $this->assertEmpty($mob->getSkills());
    }

    public function testMobSpeedRange()
    {
        $mob = MobFactory::getMob();
        $this->assertThat(
            $mob->getSpeed(),
            $this->logicalAnd(
                $this->greaterThan(40),
                $this->lessThan(60)
            )
        );
    }
}
