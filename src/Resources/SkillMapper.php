<?php


namespace App\Resources;


use App\Entity\Skill;

class SkillMapper
{
    public static function getSkillsForPlayers()
    {
        return [
            'Orderus' => [
                [
                    'name' => 'Rapid Strike',
                    'percentage' => 10,
                    'value' => 2,
                    'type' => Skill::TYPE_ATTACK,
                    'strategy' => Skill::STRATEGY_ATTACK_MULTIPLE
                ],
                [
                    'name' => 'Magic Shield',
                    'percentage' => 20,
                    'value' => 2,
                    'type' => Skill::TYPE_DEFENCE,
                    'strategy' => Skill::STRATEGY_BLOCK_MULTIPLE
                ]
            ]
        ];
    }
}