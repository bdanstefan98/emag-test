<?php


namespace App\Factory;

use App\Entity\Player;

class MobFactory extends AbstractFactory
{
    public static function getMob()
    {
        $mob = new Player();

        $mob->setName('General mob');
        $mob->setDefence(mt_rand(40,59) + ( mt_rand(0,100) / 100 ));
        $mob->setLuck(mt_rand(25,39) + ( mt_rand(0,100) / 100 ));
        $mob->setStrength(mt_rand(60,89) + ( mt_rand(0,100) / 100 ));
        $mob->setSpeed(mt_rand(40,59) + ( mt_rand(0,100) / 100 ));
        $mob->setHealth(mt_rand(60,89) + ( mt_rand(0,100) / 100 ));

        return $mob;
    }
}