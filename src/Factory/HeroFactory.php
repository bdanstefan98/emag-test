<?php


namespace App\Factory;

use App\Entity\Player;
use App\Entity\Skill;

class HeroFactory extends AbstractFactory
{
    public static function getHero()
    {
        $hero = new Player();

        $hero->setName('Orderus');
        $hero->setDefence(mt_rand(45,54) + ( mt_rand(0,100) / 100 ));
        $hero->setLuck(mt_rand(10,29) + ( mt_rand(0,100) / 100 ));
        $hero->setStrength(mt_rand(70,79) + ( mt_rand(0,100) / 100 ));
        $hero->setSpeed(mt_rand(40,49) + ( mt_rand(0,100) / 100 ));
        $hero->setHealth(mt_rand(70,99) + ( mt_rand(0,100) / 100 ));

        $skills = self::getSkills($hero)->getValues();

        /** @var Skill $skill */
        foreach($skills as $skill) {
            $hero->addSkill($skill);
        }

        return $hero;
    }
}