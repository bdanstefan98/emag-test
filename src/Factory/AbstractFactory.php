<?php


namespace App\Factory;


use App\Entity\Player;
use App\Entity\Skill;
use App\Resources\SkillMapper;
use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractFactory
{
    /**
     * @param Player $player
     *
     * @return ArrayCollection $skills
     */
    public static function getSkills(&$player)
    {
        $heroSkills = new ArrayCollection();

        foreach(SkillMapper::getSkillsForPlayers()[$player->getName()] as $skills) {
            $newSkill = new Skill();

            foreach($skills as $property => $skill) {
                $setter = 'set' . ucfirst($property);
                $newSkill -> $setter($skill) ;
            }

            $heroSkills->add($newSkill);
        }

        return $heroSkills;
    }
}