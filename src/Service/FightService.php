<?php


namespace App\Service;


use App\Entity\Player;
use App\Entity\Skill;

class FightService
{
    /** given the fact that the counting will start from round 0 */
    const MAX_NUMBER_OF_ROUNDS = 19;

    public function getFight(Player $hero, Player $mob)
    {
        $fightOrder = $this->determineOrder($hero, $mob);

        $numberOfRounds = 0;

        while ( $numberOfRounds < self::MAX_NUMBER_OF_ROUNDS &&
                 $mob->getHealth() > 0 &&
                 $hero->getHealth() > 0
        ) {
            echo "\nRound no." . ( $numberOfRounds + 1 ). "\n";
            $luck = mt_rand(0, 100);

            /** checking if the defending player gets lucky. if he gets lucky, the attacker will miss its attack for this round */
            if($luck < $fightOrder[1]->getLuck()) {
                echo $fightOrder[1]->getName() . " gets lucky and his opponent will miss. He is now the attacker! \n";
                $this->endRound($fightOrder,$numberOfRounds);
                continue 1;
            }

            /** first the defensive skills */
            if ( $this->getDefendingSkills($fightOrder) ) {
                $this->endRound($fightOrder,$numberOfRounds);
                continue 1;
            }

            /** now the attacking skills */
            if ( $this->getAttackingSkills($fightOrder) ) {
                $this->endRound($fightOrder,$numberOfRounds);
                continue 1;
            }

            /** normal attack */
            $damage = $fightOrder[0]->getStrength() - $fightOrder[1]->getDefence();
            $this->attack($fightOrder, $damage);

            $this->endRound($fightOrder,$numberOfRounds);
        }
    }

    /**
     * @param $fightOrder array
     * @param $numberOfRounds int
     */
    private function endRound(&$fightOrder, &$numberOfRounds) {
        $fightOrder = array_reverse($fightOrder);
        $numberOfRounds++;
    }

    /**
     * @param $mob Player
     * @param $hero Player
     *
     * @return array
     */
    private function determineOrder($hero, $mob) {
        if ( $hero->getSpeed() > $mob->getSpeed() ) {
            $fightOrder = [$hero, $mob];
        } elseif ( $mob->getSpeed() > $hero->getSpeed() ) {
            $fightOrder = [$mob, $hero];
        } else {
            $mob->getLuck() > $hero->getLuck() ? $fightOrder = [$mob, $hero] : $fightOrder = [$hero, $mob];
        }

        return $fightOrder;
    }

    /**
     * @param $fightOrder array
     * @return bool
     */
    private function getDefendingSkills($fightOrder)
    {
        /** checking defending skills (for the defender obv)*/

        /** @var Skill $skill */
        foreach($fightOrder[1]->getSkills() as $skill) {
            if (Skill::TYPE_DEFENCE === $skill->getType()) {
                $skillLuck = mt_rand(0, 100);
                /** calculating probability of blocking damage */
                if ($skill->getPercentage() > $skillLuck) {
                    if ( Skill::STRATEGY_BLOCK_MULTIPLE === $skill->getStrategy() ) {
                        /** calculating damage, but blocking part of it. */
                        $damage = ( $fightOrder[0]->getStrength() - $fightOrder[1]->getDefence() ) / $skill->getValue() ;
                        echo $skill->getName() . ' was used by the champion ' . $fightOrder[1]->getName() . ". He will now take 1 / " . $skill->getValue() . " damage than usual! \n";
                        $this->attack($fightOrder,$damage);
                        return true;
                    } else {
                        /** this is for extending to multiple types of defence */
                        continue 1;
                    }
                } else {
                    /** meaning that the player did not get lucky with that skill */
                    continue 1;
                }
            }
        }

        return false;
    }

    /**
     * @param $fightOrder array
     * @return bool
     */
    private function getAttackingSkills($fightOrder)
    {
        /** checking defending skills (for the defender obv)*/

        /** @var Skill $skill */
        foreach($fightOrder[0]->getSkills() as $skill) {
            if (Skill::TYPE_ATTACK === $skill->getType()) {
                $skillLuck = mt_rand(0, 100);
                /** calculating probability of blocking damage */
                if ($skill->getPercentage() > $skillLuck) {
                    if ( Skill::STRATEGY_ATTACK_MULTIPLE === $skill->getStrategy() ) {
                        /** calculating damage, but attacking more times */
                        $damage = ( $fightOrder[0]->getStrength() - $fightOrder[1]->getDefence() ) * $skill->getValue() ;
                        echo $skill->getName() . ' was used by the champion ' . $fightOrder[0]->getName() . ". He will now deal " . $skill->getValue() . " times the damage than as usual! \n";
                        $this->attack($fightOrder,$damage);
                        return true;
                    } else {
                        /** this is for extending to multiple types of attacks */
                        continue 1;
                    }
                } else {
                    /** meaning that the player did not get lucky with that skill */
                    continue 1;
                }
            }
        }

        return false;
    }

    /**
     * @param $fightOrder array
     * @param $damage float
     */
    private function attack(&$fightOrder, $damage) {
        $fightOrder[1]->setHealth( $fightOrder[1]->getHealth() - $damage );
        if($fightOrder[1]->getHealth() <= 0) {
            echo 'The fight is over and ' . $fightOrder[0]->getName() . " is the winner, dealing the final blow with $damage attack \n";
        } else {
            echo $fightOrder[1]->getName() . " has taken $damage damage and is now his turn to attack! He has " . $fightOrder[1]->getHealth() . " hp left!\n";
        }
    }
}