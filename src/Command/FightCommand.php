<?php

namespace App\Command;

use App\Factory\HeroFactory;
use App\Factory\MobFactory;
use App\Service\FightService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FightCommand extends Command
{
    protected static $defaultName = 'fight:command';

    private $fightService;

    public function __construct(FightService $fightService, $name = null)
    {
        parent::__construct($name);
        $this->fightService = $fightService;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $hero = HeroFactory::getHero();
        $mob = MobFactory::getMob();

        $this->fightService->getFight($hero, $mob);

        return 0;
    }
}
